package com.javasampleapproach.formsubmission.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.javasampleapproach.formsubmission.model.Customer;
import com.javasampleapproach.formsubmission.model.NavPage;
import com.javasampleapproach.formsubmission.model.Tabs;
import com.javasampleapproach.formsubmission.service.DataEntryService;
import com.javasampleapproach.formsubmission.util.DriveQuickstart;
import com.javasampleapproach.formsubmission.util.Utils;

@Controller
public class SimpleWebController {

	Logger log = LoggerFactory.getLogger(this.getClass());

	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String customerForm(Model model) {
		model.addAttribute("customer", new Customer());
		return "form";
	}

	@RequestMapping(value = "/formEntraceExam", method = RequestMethod.GET)
	public String customerFormEntrance(Model model) {
		model.addAttribute("customer", new Customer());
		return "formEntraceExam";
	}

	@RequestMapping(value = "/formEntraceExam", method = RequestMethod.POST)
	public Customer customerSubmitEntrance(@ModelAttribute Customer customer, Model model) {
		model.addAttribute("customer", customer);

		List<String> tableColumns = Utils.getTableColumns();
		List<String> columnDataType = Utils.getColumnDataType();
		DataEntryService dataEntryService = new DataEntryService();
		customer = dataEntryService.addDataIntoEntranceExams(tableColumns, columnDataType, customer);
		if (customer.getError() == null)
			customer.setError("Record inserted successfully. :" + customer.getPaperName());
		return customer;
	}

	@RequestMapping(value = "/formSchool", method = RequestMethod.GET)
	public String customerFormSchool(Model model) {
		model.addAttribute("customer", new Customer());
		return "formSchool";
	}

	@RequestMapping(value = "/formUniversity", method = RequestMethod.GET)
	public String customerFormUniversity(Model model) {
		model.addAttribute("customer", new Customer());
		return "formUniversity";
	}

	@RequestMapping(value = "/formUniversity", method = RequestMethod.POST)
	public Customer customerSubmitUniversity(@ModelAttribute Customer customer, Model model) {
		model.addAttribute("customer", customer);

		List<String> tableColumns = Utils.getTableColumns();
		List<String> columnDataType = Utils.getColumnDataType();
		DataEntryService dataEntryService = new DataEntryService();
		customer = dataEntryService.addDataIntoUniversity(tableColumns, columnDataType, customer);
		if (customer.getError() == null)
			customer.setError("Record inserted successfully. :" + customer.getPaperName());
		return customer;
	}

	@RequestMapping(value = "/formGovernment", method = RequestMethod.GET)
	public String customerFormGovernment(Model model) {
		model.addAttribute("customer", new Customer());
		return "formGovernment";
	}

	@RequestMapping(value = "/formGovernment", method = RequestMethod.POST)
	public Customer customerSubmitGovernment(@ModelAttribute Customer customer, Model model) {
		model.addAttribute("customer", customer);

		List<String> tableColumns = Utils.getTableColumns();
		List<String> columnDataType = Utils.getColumnDataType();
		DataEntryService dataEntryService = new DataEntryService();
		customer = dataEntryService.addDataIntoGovernment(tableColumns, columnDataType, customer);
		if (customer.getError() == null)
			customer.setError("Record inserted successfully. :" + customer.getPaperName());
		return customer;
	}

	@RequestMapping(value = "/formSchool", method = RequestMethod.POST)
	public Customer customerSubmitSchool(@ModelAttribute Customer customer, Model model) {
		model.addAttribute("customer", customer);

		List<String> tableColumns = Utils.getTableColumns();
		List<String> columnDataType = Utils.getColumnDataType();
		DataEntryService dataEntryService = new DataEntryService();
		customer = dataEntryService.addDataIntoSchool(tableColumns, columnDataType, customer);
		if (customer.getError() == null)
			customer.setError("Record inserted successfully. :" + customer.getPaperName());
		return customer;
	}

	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public Customer customerSubmit(@ModelAttribute Customer customer, Model model) {

		model.addAttribute("customer", customer);

		List<String> tableColumns = Utils.getTableColumns();
		List<String> columnDataType = Utils.getColumnDataType();
		DataEntryService dataEntryService = new DataEntryService();

		customer = dataEntryService.addDataIntoEntranceExams(tableColumns, columnDataType, customer);

		if (customer.getError() == null)
			customer.setError("Record inserted successfully. :" + customer.getPaperName());
		return customer;
	}

	@RequestMapping(value = "/formPlacement", method = RequestMethod.GET)
	public String customerFormPlacement(Model model) {
		model.addAttribute("customer", new Customer());
		return "formPlacement";
	}

	@RequestMapping(value = "/formPlacement", method = RequestMethod.POST)
	public Customer customerSubmitPlacement(@ModelAttribute Customer customer, Model model) {
		model.addAttribute("customer", customer);

		List<String> tableColumns = Utils.getTableColumns();
		List<String> columnDataType = Utils.getColumnDataType();
		DataEntryService dataEntryService = new DataEntryService();
		customer = dataEntryService.addDataIntoPlacement(tableColumns, columnDataType, customer);
		if (customer.getError() == null)
			customer.setError("Record inserted successfully. :" + customer.getPaperName());
		return customer;
	}

	@RequestMapping(value = "/formInternational", method = RequestMethod.GET)
	public String customerFormInternational(Model model) {
		model.addAttribute("customer", new Customer());
		return "formInternational";
	}

	@RequestMapping(value = "/formInternational", method = RequestMethod.POST)
	public Customer customerSubmitInternational(@ModelAttribute Customer customer, Model model) {
		model.addAttribute("customer", customer);

		List<String> tableColumns = Utils.getTableColumns();
		List<String> columnDataType = Utils.getColumnDataType();
		DataEntryService dataEntryService = new DataEntryService();
		customer = dataEntryService.addDataIntoInternational(tableColumns, columnDataType, customer);
		if (customer.getError() == null)
			customer.setError("Record inserted successfully. :" + customer.getPaperName());
		return customer;
	}

//	@RequestMapping(value = "/formDrive", method = RequestMethod.GET)
//	public String customerFormDrive(Model model) {
//		model.addAttribute("customer", new Customer());
//		return "formDrive";
//	}
//
//	@RequestMapping(value = "/formDrive", method = RequestMethod.POST)
//	public Customer customerForDrive(@ModelAttribute Customer customer, Model model) {
//
//		DriveQuickstart driveQuickstart = new DriveQuickstart();
//		List<Customer> customers = driveQuickstart.main(customer);
//
//		if (customers != null && customers.size() > 0) {
//
//			model.addAttribute("customer", customer);
//
//			List<String> tableColumns = Utils.getTableColumns();
//			List<String> columnDataType = Utils.getColumnDataType();
//			DataEntryService dataEntryService = new DataEntryService();
//			for (Customer customer2 : customers) {
//				customer = dataEntryService.addDataIntoTwoStageDrive(tableColumns, columnDataType, customer2);
//			}
//			if (customer.getError() == null)
//				customer.setError("Record inserted successfully. :" + customer.getPaperName());
//		}
//		return customer;
//	}

	@RequestMapping(value = "/formDrive", method = RequestMethod.GET)
	public String customerFormDrive(Model model) {
		model.addAttribute("navPage", new NavPage());
		return "formDrive";
	}

	@RequestMapping(value = "/formDrive", method = RequestMethod.POST)
	public NavPage customerForDrive(@ModelAttribute NavPage customer, Model model) {

		DriveQuickstart driveQuickstart = new DriveQuickstart();
		List<NavPage> customers = driveQuickstart.main(customer);

		if (customers != null && customers.size() > 0) {

			model.addAttribute("customer", customer);

			List<String> tableColumns = Utils.getTableColumns();
			List<String> columnDataType = Utils.getColumnDataType();
			DataEntryService dataEntryService = new DataEntryService();
			for (NavPage customer2 : customers) {
				customer = dataEntryService.addData(tableColumns, columnDataType, customer2);
			}
			if (customer.getError() == null)
				customer.setError("Record inserted successfully. :" + customer.getPaperName());
		}
		return customer;
	}

	@RequestMapping(value = "/formDriveUniversity", method = RequestMethod.GET)
	public String customerFormDriveUniveristy(Model model) {
		model.addAttribute("customer", new Customer());
		return "formDriveUniversity";
	}

	@RequestMapping(value = "/formDriveInter", method = RequestMethod.GET)
	public String customerFormDriveInter(Model model) {
		model.addAttribute("customer", new Customer());
		return "formDriveInter";
	}
	
	@RequestMapping(value = "/formTabs", method = RequestMethod.GET)
	public String customerFormTabs(Model model) {
		model.addAttribute("tabs", new Tabs());
		return "formTabs";
	}

	@RequestMapping(value = "/formTabs", method = RequestMethod.POST)
	public Tabs customerSubmitTabs(@ModelAttribute Tabs tabs, Model model) {
		model.addAttribute("tabs", tabs);

		List<String> tableColumns = Utils.getTabsTableColumns();
		List<String> columnDataType = Utils.getTabsColumnDataType();
		DataEntryService dataEntryService = new DataEntryService();
		tabs = dataEntryService.addDataIntoTabs(tableColumns, columnDataType, tabs);
		if (tabs.getError() == null)
			tabs.setError("Record inserted successfully. :" + tabs.getHomePageId());
		return tabs;
	}
}
