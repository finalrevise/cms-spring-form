package com.javasampleapproach.formsubmission.model;

public class Customer {

	private long homePageId;
	private String unversityName;
	private String course;
	private String year;
	private String courseDetails;
	private String syllabus;
	private String subName;
	private String subUrl;
	private String examName;
	private String examDetails;
	private String position;
	private String paperName;
	private String paperUrl;
	private String boardName;
	private String classTemp;
	private String companyName;
	private String companyDetails;
	private String unversityDetails;
	private String sem;
	private String tempName = "";
	private String tempPaperUrl = "";
	private String actualPaperName = "";
	private String error;
	private String folderId;
	private String cat;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getActualPaperName() {
		return actualPaperName;
	}

	public String getUnversityDetails() {
		return unversityDetails;
	}

	public void setUnversityDetails(String unversityDetails) {
		this.unversityDetails = unversityDetails;
	}

	public String getSem() {
		return sem;
	}

	public void setSem(String sem) {
		this.sem = sem;
	}

	public void setActualPaperName(String actualPaperName) {
		this.actualPaperName = actualPaperName;
	}

	public String getTempName() {
		return tempName;
	}

	public void setTempName(String tempName) {
		this.tempName = tempName;
	}

	public String getTempPaperUrl() {
		return tempPaperUrl;
	}

	public void setTempPaperUrl(String tempPaperUrl) {
		this.tempPaperUrl = tempPaperUrl;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyDetails() {
		return companyDetails;
	}

	public void setCompanyDetails(String companyDetails) {
		this.companyDetails = companyDetails;
	}

	public String getBoardName() {
		return boardName;
	}

	public void setBoardName(String boardName) {
		this.boardName = boardName;
	}

	public String getClassTemp() {
		return classTemp;
	}

	public void setClassTemp(String classTemp) {
		this.classTemp = classTemp;
	}

	public String getExamName() {
		return examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public String getExamDetails() {
		return examDetails;
	}

	public void setExamDetails(String examDetails) {
		this.examDetails = examDetails;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getPaperName() {
		return paperName;
	}

	public void setPaperName(String paperName) {
		this.paperName = paperName;
	}

	public String getPaperUrl() {
		return paperUrl;
	}

	public void setPaperUrl(String paperUrl) {
		this.paperUrl = paperUrl;
	}

	public long getHomePageId() {
		return homePageId;
	}

	public void setHomePageId(long homePageId) {
		this.homePageId = homePageId;
	}

	public String getUnversityName() {
		return unversityName;
	}

	public void setUnversityName(String unversityName) {
		this.unversityName = unversityName;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getCourseDetails() {
		return courseDetails;
	}

	public void setCourseDetails(String courseDetails) {
		this.courseDetails = courseDetails;
	}

	public String getSyllabus() {
		return syllabus;
	}

	public void setSyllabus(String syllabus) {
		this.syllabus = syllabus;
	}

	public String getSubName() {
		return subName;
	}

	public void setSubName(String subName) {
		this.subName = subName;
	}

	public String getSubUrl() {
		return subUrl;
	}

	public void setSubUrl(String subUrl) {
		this.subUrl = subUrl;
	}

	public String getFolderId() {
		return folderId;
	}

	public void setFolderId(String folderId) {
		this.folderId = folderId;
	}

	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}

}