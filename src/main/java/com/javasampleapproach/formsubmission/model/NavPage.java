package com.javasampleapproach.formsubmission.model;

public class NavPage {

	private int homePageId;
	private String title1;
	private String title2;
	private String title3;
	private String title4;
	private String title5;
	private String title6;
	private String title1Desc;
	private String title2Desc;
	private String title3Desc;
	private String title4Desc;
	private String title5Desc;
	private String title6Desc;
	private String paperName;
	private String paperUrl;
	private String tempName = "";
	private String tempPaperUrl = "";
	private Integer cat;

	private String folderId;
	private String error;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public int getHomePageId() {
		return homePageId;
	}

	public void setHomePageId(int homePageId) {
		this.homePageId = homePageId;
	}

	public String getTitle1() {
		return title1;
	}

	public void setTitle1(String title1) {
		this.title1 = title1;
	}

	public String getTitle2() {
		return title2;
	}

	public void setTitle2(String title2) {
		this.title2 = title2;
	}

	public String getTitle3() {
		return title3;
	}

	public void setTitle3(String title3) {
		this.title3 = title3;
	}

	public String getTitle4() {
		return title4;
	}

	public void setTitle4(String title4) {
		this.title4 = title4;
	}

	public String getTitle5() {
		return title5;
	}

	public void setTitle5(String title5) {
		this.title5 = title5;
	}

	public String getTitle6() {
		return title6;
	}

	public void setTitle6(String title6) {
		this.title6 = title6;
	}

	public String getFolderId() {
		return folderId;
	}

	public void setFolderId(String folderId) {
		this.folderId = folderId;
	}

	public String getTitle1Desc() {
		return title1Desc;
	}

	public void setTitle1Desc(String title1Desc) {
		this.title1Desc = title1Desc;
	}

	public String getTitle2Desc() {
		return title2Desc;
	}

	public void setTitle2Desc(String title2Desc) {
		this.title2Desc = title2Desc;
	}

	public String getTitle3Desc() {
		return title3Desc;
	}

	public void setTitle3Desc(String title3Desc) {
		this.title3Desc = title3Desc;
	}

	public String getTitle4Desc() {
		return title4Desc;
	}

	public void setTitle4Desc(String title4Desc) {
		this.title4Desc = title4Desc;
	}

	public String getTitle5Desc() {
		return title5Desc;
	}

	public void setTitle5Desc(String title5Desc) {
		this.title5Desc = title5Desc;
	}

	public String getTitle6Desc() {
		return title6Desc;
	}

	public void setTitle6Desc(String title6Desc) {
		this.title6Desc = title6Desc;
	}

	public String getPaperName() {
		return paperName;
	}

	public void setPaperName(String paperName) {
		this.paperName = paperName;
	}

	public String getPaperUrl() {
		return paperUrl;
	}

	public void setPaperUrl(String paperUrl) {
		this.paperUrl = paperUrl;
	}

	public String getTempName() {
		return tempName;
	}

	public void setTempName(String tempName) {
		this.tempName = tempName;
	}

	public String getTempPaperUrl() {
		return tempPaperUrl;
	}

	public void setTempPaperUrl(String tempPaperUrl) {
		this.tempPaperUrl = tempPaperUrl;
	}

	public Integer getCat() {
		return cat;
	}

	public void setCat(Integer cat) {
		this.cat = cat;
	}

}
