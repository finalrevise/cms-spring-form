package com.javasampleapproach.formsubmission.model;

public class Tabs {

	private long homePageId;
	private String tabs;
	private String addAssIds;
	private String removeAssIds;
	private String error;

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public long getHomePageId() {
		return homePageId;
	}

	public void setHomePageId(long homePageId) {
		this.homePageId = homePageId;
	}

	public String getTabs() {
		return tabs;
	}

	public void setTabs(String tabs) {
		this.tabs = tabs;
	}

	public String getAddAssIds() {
		return addAssIds;
	}

	public void setAddAssIds(String addAssIds) {
		this.addAssIds = addAssIds;
	}

	public String getRemoveAssIds() {
		return removeAssIds;
	}

	public void setRemoveAssIds(String removeAssIds) {
		this.removeAssIds = removeAssIds;
	}
	
}