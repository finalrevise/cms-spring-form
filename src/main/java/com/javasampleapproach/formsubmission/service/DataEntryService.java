package com.javasampleapproach.formsubmission.service;

import java.util.ArrayList;
import java.util.List;

import com.javasampleapproach.formsubmission.manager.DbConnManager;
import com.javasampleapproach.formsubmission.model.Customer;
import com.javasampleapproach.formsubmission.model.NavPage;
import com.javasampleapproach.formsubmission.model.Tabs;
import com.javasampleapproach.formsubmission.util.Utils;

public class DataEntryService {

	public Customer addDataIntoEntranceExams(List<String> tableColumns, List<String> columnDataType,
			Customer customer) {
		DbConnManager connManager = DbConnManager.getInstance();
		Long navId = connManager.pageAvaData("app_nav_page", "2", "parent_page_id");
		if (navId == null) {
			navId = basicData(tableColumns, columnDataType, "All Competitive Exams Previous Year Question Papers",
					"All Competitive/Entrance Exams Previous Year Question Papers for Bachelors/Masters", 2l);
		}
		Long pageId = connManager.pageAvaData("app_nav_page", customer.getExamName(), "nav_page_title");
		if (pageId == null) {
			List<String> columnDataValues = new ArrayList<>();
			pageId = System.currentTimeMillis();
			columnDataValues.add("" + pageId);
			columnDataValues.add(customer.getExamDetails());
			columnDataValues.add("Home#All Competitive Exams Previous Year Question Papers#" + customer.getExamName());
			columnDataValues.add("2#" + navId + "#" + pageId);
			columnDataValues.add(customer.getExamName());
			columnDataValues.add("" + navId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		customer = addPageData(pageId, customer, "Entrance Exams");
		return customer;
	}

	public Customer addDataIntoGovernment(List<String> tableColumns, List<String> columnDataType, Customer customer) {
		DbConnManager connManager = DbConnManager.getInstance();
		Long navId = connManager.pageAvaData("app_nav_page", "4", "parent_page_id");
		if (navId == null) {
			navId = basicData(tableColumns, columnDataType, "Government Jobs", "All Government Exam Papers", 4l);
		}
		Long pageId = connManager.pageAvaData("app_nav_page", customer.getExamName(), "nav_page_title");
		if (pageId == null) {
			List<String> columnDataValues = new ArrayList<>();
			pageId = System.currentTimeMillis();
			columnDataValues.add("" + pageId);
			columnDataValues.add(customer.getExamDetails());
			columnDataValues.add("Home#All Government Exam Papers#" + customer.getExamName());
			columnDataValues.add("4#" + navId + "#" + pageId);
			columnDataValues.add(customer.getExamName());
			columnDataValues.add("" + navId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		customer = addPageData(pageId, customer, "Government Jobs");
		return customer;
	}

	public Customer addDataIntoPlacement(List<String> tableColumns, List<String> columnDataType, Customer customer) {
		DbConnManager connManager = DbConnManager.getInstance();
		Long navId = connManager.pageAvaData("app_nav_page", "5", "parent_page_id");
		if (navId == null) {
			navId = basicData(tableColumns, columnDataType, "Placement Papers", "Companies Interview Questions", 5l);
		}
		Long pageId = connManager.pageAvaData("app_nav_page", customer.getCompanyName(), "nav_page_title");
		if (pageId == null) {
			List<String> columnDataValues = new ArrayList<>();
			pageId = System.currentTimeMillis();
			columnDataValues.add("" + pageId);
			columnDataValues.add(customer.getCompanyDetails());
			columnDataValues.add("Home#Placement Papers#" + customer.getCompanyName());
			columnDataValues.add("5#" + navId + "#" + pageId);
			columnDataValues.add(customer.getCompanyName());
			columnDataValues.add("" + navId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		customer = addPageData(pageId, customer, "Placement Papers");
		return customer;
	}

	public Customer addDataIntoSchool(List<String> tableColumns, List<String> columnDataType, Customer customer) {
		Long navId = DbConnManager.getInstance().pageAvaData("app_nav_page", "1", "parent_page_id");
		if (navId == null) {
			navId = basicData(tableColumns, columnDataType, "School Boards", "State and Centre Board", 1l);
		}
		Long boardId = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getBoardName(),
				"nav_page_title");
		if (boardId == null) {
			List<String> columnDataValues = new ArrayList<>();
			boardId = System.currentTimeMillis();
			columnDataValues.add("" + boardId);
			columnDataValues.add(customer.getExamDetails());
			columnDataValues.add("Home#School Boards#" + customer.getBoardName());
			columnDataValues.add("1#" + navId + "#" + boardId);
			columnDataValues.add(customer.getBoardName());
			columnDataValues.add("" + navId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		Long id = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getClassTemp(), "nav_page_title",
				"parent_page_id", boardId);
		if (id == null) {
			List<String> columnDataValues = new ArrayList<>();
			id = System.currentTimeMillis();
			columnDataValues.add("" + id);
			columnDataValues.add(customer.getSyllabus());
			columnDataValues.add("Home#School Boards#" + customer.getBoardName() + "#" + customer.getClassTemp());
			columnDataValues.add("1#" + navId + "#" + boardId + "#" + id);
			columnDataValues.add(customer.getClassTemp());
			columnDataValues.add("" + boardId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		customer = addPageData(id, customer, customer.getBoardName());
		return customer;
	}

	public Customer addDataIntoUniversity(List<String> tableColumns, List<String> columnDataType, Customer customer) {
		Long navId = DbConnManager.getInstance().pageAvaData("app_nav_page", "3", "parent_page_id");
		if (navId == null) {
			navId = basicData(tableColumns, columnDataType, "University", "Last Semester Papers are online", 3l);
		}
		Long uniId = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getUnversityName(),
				"nav_page_title");
		if (uniId == null) {
			List<String> columnDataValues = new ArrayList<>();
			uniId = System.currentTimeMillis();
			columnDataValues.add("" + uniId);
			columnDataValues.add(customer.getUnversityDetails());
			columnDataValues.add("Home#University#" + customer.getUnversityName());
			columnDataValues.add("3#" + navId + "#" + uniId);
			columnDataValues.add(customer.getUnversityName());
			columnDataValues.add("" + navId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		Long courseId = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getCourse(), "nav_page_title",
				"parent_page_id", uniId);
		if (courseId == null) {
			List<String> columnDataValues = new ArrayList<>();
			courseId = System.currentTimeMillis();
			columnDataValues.add("" + courseId);
			columnDataValues.add(customer.getCourseDetails());
			columnDataValues.add("Home#University#" + customer.getUnversityName() + "#" + customer.getCourse());
			columnDataValues.add("3#" + navId + "#" + uniId + "#" + courseId);
			columnDataValues.add(customer.getCourse());
			columnDataValues.add("" + uniId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		Long id = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getSem(), "nav_page_title",
				"parent_page_id", courseId);
		if (id == null) {
			List<String> columnDataValues = new ArrayList<>();
			id = System.currentTimeMillis();
			columnDataValues.add("" + id);
			columnDataValues.add(customer.getSyllabus());
			columnDataValues.add("Home#University#" + customer.getUnversityName() + "#" + customer.getCourse() + "#"
					+ customer.getSem());
			columnDataValues.add("3#" + navId + "#" + uniId + "#" + courseId + "#" + id);
			columnDataValues.add(customer.getSem());
			columnDataValues.add("" + courseId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		customer = addPageData(id, customer, customer.getCourse());
		return customer;
	}

	public Customer addDataIntoTwoStageDrive(List<String> tableColumns, List<String> columnDataType,
			Customer customer) {
		Long navId = DbConnManager.getInstance().pageAvaData("app_nav_page", "6", "parent_page_id");
		if (navId == null) {
			navId = basicData(tableColumns, columnDataType, "International Competition", "International Exams Papers",
					6l);
		}
		Long boardId = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getBoardName(),
				"nav_page_title");
		if (boardId == null) {
			List<String> columnDataValues = new ArrayList<>();
			boardId = System.currentTimeMillis();
			columnDataValues.add("" + boardId);
			columnDataValues.add(customer.getExamDetails());
			columnDataValues.add("Home#International Exams Papers#" + customer.getBoardName());
			columnDataValues.add("6#" + navId + "#" + boardId);
			columnDataValues.add(customer.getBoardName());
			columnDataValues.add("" + navId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		Long id = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getClassTemp(), "nav_page_title",
				"parent_page_id", boardId);
		if (id == null) {
			List<String> columnDataValues = new ArrayList<>();
			id = System.currentTimeMillis();
			columnDataValues.add("" + id);
			columnDataValues.add(customer.getSyllabus());
			columnDataValues
					.add("Home#International Exams Papers#" + customer.getBoardName() + "#" + customer.getClassTemp());
			columnDataValues.add("6#" + navId + "#" + boardId + "#" + id);
			columnDataValues.add(customer.getClassTemp());
			columnDataValues.add("" + boardId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		customer = addPageData(id, customer, customer.getBoardName());
		return customer;
	}

	public Customer addDataIntoThreeStageDrive(List<String> tableColumns, List<String> columnDataType,
			Customer customer) {

		Long navId = DbConnManager.getInstance().pageAvaData("app_nav_page", "6", "parent_page_id");
		if (navId == null) {
			navId = basicData(tableColumns, columnDataType, "International Competition", "International Exams Papers",
					6l);
		}
		Long boardId = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getBoardName(),
				"nav_page_title");
		if (boardId == null) {
			List<String> columnDataValues = new ArrayList<>();
			boardId = System.currentTimeMillis();
			columnDataValues.add("" + boardId);
			columnDataValues.add(customer.getExamDetails());
			columnDataValues.add("Home#International Exams Papers#" + customer.getBoardName());
			columnDataValues.add("6#" + navId + "#" + boardId);
			columnDataValues.add(customer.getBoardName());
			columnDataValues.add("" + navId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		Long id = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getClassTemp(), "nav_page_title",
				"parent_page_id", boardId);
		if (id == null) {
			List<String> columnDataValues = new ArrayList<>();
			id = System.currentTimeMillis();
			columnDataValues.add("" + id);
			columnDataValues.add(customer.getSyllabus());
			columnDataValues
					.add("Home#International Exams Papers#" + customer.getBoardName() + "#" + customer.getClassTemp());
			columnDataValues.add("6#" + navId + "#" + boardId + "#" + id);
			columnDataValues.add(customer.getClassTemp());
			columnDataValues.add("" + boardId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		customer = addPageData(id, customer, customer.getBoardName());
		return customer;

	}

	public Customer addDataIntoInternational(List<String> tableColumns, List<String> columnDataType,
			Customer customer) {
		DbConnManager connManager = DbConnManager.getInstance();
		Long navId = connManager.pageAvaData("app_nav_page", "6", "parent_page_id");
		if (navId == null) {
			navId = basicData(tableColumns, columnDataType, "International Competition", "International Exams Papers",
					6l);
		}
		Long pageId = connManager.pageAvaData("app_nav_page", customer.getExamName(), "nav_page_title");
		if (pageId == null) {
			List<String> columnDataValues = new ArrayList<>();
			pageId = System.currentTimeMillis();
			columnDataValues.add("" + pageId);
			columnDataValues.add(customer.getExamDetails());
			columnDataValues.add("Home#International Exams Papers#" + customer.getExamName());
			columnDataValues.add("6#" + navId + "#" + pageId);
			columnDataValues.add(customer.getExamName());
			columnDataValues.add("" + navId);
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		}
		customer = addPageData(pageId, customer, "Placement Papers");
		return customer;
	}

	public Customer addPageData(Long id, Customer customer, String actualTitle) {
		List<String> tableColumns = new ArrayList<>();
		tableColumns.add("nav_page_id");
		tableColumns.add("page_doc_link");
		tableColumns.add("page_item_name");
		tableColumns.add("actual_item_name");
		tableColumns.add("reference_item_name");
		tableColumns.add("temp_doc_link");
		List<String> columnDataType = new ArrayList<>();
		columnDataType.add("LONG");
		columnDataType.add("STRING");
		columnDataType.add("STRING");
		columnDataType.add("STRING");
		columnDataType.add("STRING");
		columnDataType.add("STRING");
		List<String> columnDataValues = new ArrayList<>();
		columnDataValues.add("" + id);
		columnDataValues.add(customer.getPaperUrl());
		columnDataValues.add(customer.getPaperName());
		columnDataValues.add(actualTitle);
		String temp = customer.getTempName() == ""
				? customer.getExamName() != null ? customer.getExamName()
						: customer.getUnversityName() != null ? customer.getUnversityName() : customer.getTempName()
				: customer.getTempName();
		columnDataValues.add(temp);
		columnDataValues.add(customer.getTempPaperUrl());
		try {
			DbConnManager.getInstance().execute("app_nav_page_data", tableColumns, columnDataType, columnDataValues);
		} catch (Exception e) {
			customer.setError(e.getMessage());
		}
		return customer;
	}

	public Long basicData(List<String> tableColumns, List<String> columnDataType, String title, String desc,
			Long parentId) {
		List<String> columnDataValues = new ArrayList<>();
		Long id = System.currentTimeMillis();
		columnDataValues.add("" + id);
		columnDataValues.add(desc);
		columnDataValues.add("Home#" + title);
		columnDataValues.add(parentId + "#" + id);
		columnDataValues.add(title);
		columnDataValues.add("" + parentId);
		try {
			DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	public NavPage addData(List<String> tableColumns, List<String> columnDataType, NavPage customer) {
		String strNavIds = "";
		String strNavStrings = "";
		Long navId = DbConnManager.getInstance().pageAvaData("app_nav_page", "" + customer.getHomePageId(),
				"parent_page_id");

		Long title1BoardId = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getTitle1(),
				"nav_page_title");
		if (title1BoardId == null) {
			List<String> columnDataValues = new ArrayList<>();
			title1BoardId = System.currentTimeMillis();
			columnDataValues.add("" + title1BoardId);
			columnDataValues.add(customer.getTitle1Desc());
			strNavStrings = "Home#" + Utils.getMapping(customer.getHomePageId()) + "#" + customer.getTitle1();
			columnDataValues.add(strNavStrings);
			strNavIds = customer.getHomePageId() + "#" + navId + "#" + title1BoardId;
			columnDataValues.add(strNavIds);
			columnDataValues.add(customer.getTitle1());
			columnDataValues.add("" + navId);
			columnDataValues.add("" + customer.getCat());
			try {
				DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
			} catch (Exception e) {
				customer.setError(e.getMessage());
				return customer;
			}
		} else {
			strNavStrings = "Home#" + Utils.getMapping(customer.getHomePageId()) + "#" + customer.getTitle1();
			strNavIds = customer.getHomePageId() + "#" + navId + "#" + title1BoardId;
		}

		Long title2BoardId = -1l;

		if (customer.getTitle2() != null && customer.getTitle2().length() > 0) {
			title2BoardId = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getTitle2(),
					"nav_page_title", "parent_page_id", title1BoardId);
			if (title2BoardId == null) {
				List<String> columnDataValues = new ArrayList<>();
				title2BoardId = System.currentTimeMillis();
				columnDataValues.add("" + title2BoardId);
				columnDataValues.add(customer.getTitle2Desc());
				strNavStrings = strNavStrings + "#" + customer.getTitle2();
				columnDataValues.add(strNavStrings);
				strNavIds = strNavIds + "#" + title2BoardId;
				columnDataValues.add(strNavIds);
				columnDataValues.add(customer.getTitle2());
				columnDataValues.add("" + title1BoardId);
				columnDataValues.add("" + customer.getCat());
				try {
					DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
				} catch (Exception e) {
					customer.setError(e.getMessage());
					return customer;
				}
			} else {
				strNavStrings = strNavStrings + "#" + customer.getTitle2();
				strNavIds = strNavIds + "#" + title2BoardId;
			}
		}

		Long title3BoardId = -1l;
		if (customer.getTitle3() != null && customer.getTitle3().length() > 0) {
			title3BoardId = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getTitle3(),
					"nav_page_title", "parent_page_id", title2BoardId);
			if (title3BoardId == null) {
				List<String> columnDataValues = new ArrayList<>();
				title3BoardId = System.currentTimeMillis();
				columnDataValues.add("" + title3BoardId);
				columnDataValues.add(customer.getTitle3Desc());
				strNavStrings = strNavStrings + "#" + customer.getTitle3();
				columnDataValues.add(strNavStrings);
				strNavIds = strNavIds + "#" + title3BoardId;
				columnDataValues.add(strNavIds);
				columnDataValues.add(customer.getTitle3());
				columnDataValues.add("" + title2BoardId);
				columnDataValues.add("" + customer.getCat());
				try {
					DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
				} catch (Exception e) {
					customer.setError(e.getMessage());
					return customer;
				}
			} else {
				strNavStrings = strNavStrings + "#" + customer.getTitle3();
				strNavIds = strNavIds + "#" + title3BoardId;
			}
		}

		Long title4BoardId = -1l;
		if (customer.getTitle4() != null && customer.getTitle4().length() > 0) {
			title4BoardId = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getTitle4(),
					"nav_page_title", "parent_page_id", title3BoardId);
			if (title4BoardId == null) {
				List<String> columnDataValues = new ArrayList<>();
				title4BoardId = System.currentTimeMillis();
				columnDataValues.add("" + title4BoardId);
				columnDataValues.add(customer.getTitle4Desc());
				strNavStrings = strNavStrings + "#" + customer.getTitle4();
				columnDataValues.add(strNavStrings);
				strNavIds = strNavIds + "#" + title4BoardId;
				columnDataValues.add(strNavIds);
				columnDataValues.add(customer.getTitle4());
				columnDataValues.add("" + title3BoardId);
				columnDataValues.add("" + customer.getCat());
				try {
					DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
				} catch (Exception e) {
					customer.setError(e.getMessage());
					return customer;
				}
			} else {
				strNavStrings = strNavStrings + "#" + customer.getTitle4();
				strNavIds = strNavIds + "#" + title4BoardId;
			}
		}

		Long title5BoardId = -1l;
		if (customer.getTitle5() != null && customer.getTitle5().length() > 0) {
			title5BoardId = DbConnManager.getInstance().pageAvaData("app_nav_page", customer.getTitle5(),
					"nav_page_title", "parent_page_id", title4BoardId);
			if (title5BoardId == null) {
				List<String> columnDataValues = new ArrayList<>();
				title5BoardId = System.currentTimeMillis();
				columnDataValues.add("" + title5BoardId);
				columnDataValues.add(customer.getTitle5Desc());
				strNavStrings = strNavStrings + "#" + customer.getTitle5();
				columnDataValues.add(strNavStrings);
				strNavIds = strNavIds + "#" + title5BoardId;
				columnDataValues.add(strNavIds);
				columnDataValues.add(customer.getTitle5());
				columnDataValues.add("" + title4BoardId);
				columnDataValues.add("" + customer.getCat());
				try {
					DbConnManager.getInstance().execute("app_nav_page", tableColumns, columnDataType, columnDataValues);
				} catch (Exception e) {
					customer.setError(e.getMessage());
					return customer;
				}
			} else {
				strNavStrings = strNavStrings + "#" + customer.getTitle5();
				strNavIds = strNavIds + "#" + title5BoardId;
			}
		}

		long id = title5BoardId != -1 ? title5BoardId
				: title4BoardId != -1 ? title4BoardId
						: title3BoardId != -1 ? title3BoardId
								: title2BoardId != -1 ? title2BoardId : title1BoardId != -1 ? title1BoardId : 0l;

		String titleName = title5BoardId != null ? customer.getTitle5()
				: title4BoardId != null ? customer.getTitle4()
						: title3BoardId != null ? customer.getTitle3()
								: title2BoardId != null ? customer.getTitle2()
										: title1BoardId != null ? customer.getTitle1() : "";

		if (id != 0)
			customer = addPageData(id, customer, titleName);

		return customer;
	}

	public NavPage addPageData(Long id, NavPage customer, String actualTitle) {
		List<String> tableColumns = new ArrayList<>();
		tableColumns.add("nav_page_id");
		tableColumns.add("page_doc_link");
		tableColumns.add("page_item_name");
		tableColumns.add("actual_item_name");
		tableColumns.add("reference_item_name");
		tableColumns.add("temp_doc_link");
		List<String> columnDataType = new ArrayList<>();
		columnDataType.add("LONG");
		columnDataType.add("STRING");
		columnDataType.add("STRING");
		columnDataType.add("STRING");
		columnDataType.add("STRING");
		columnDataType.add("STRING");
		List<String> columnDataValues = new ArrayList<>();
		columnDataValues.add("" + id);
		columnDataValues.add(customer.getPaperUrl());
		columnDataValues.add(customer.getPaperName());
		columnDataValues.add(actualTitle);
		String temp = customer.getTempName() == ""
				? customer.getTitle4() == "" ? customer.getTitle4()
						: customer.getTitle3() == "" ? customer.getTitle3() : customer.getTempName()
				: customer.getTempName();
		columnDataValues.add(temp);
		columnDataValues.add(customer.getTempPaperUrl());
		try {
			DbConnManager.getInstance().execute("app_nav_page_data", tableColumns, columnDataType, columnDataValues);
		} catch (Exception e) {
			customer.setError(e.getMessage());
		}
		return customer;
	}

	public Tabs addDataIntoTabs(List<String> tableColumns, List<String> columnDataType, Tabs tabs) {
		DbConnManager connManager = DbConnManager.getInstance();
		String pageIds = connManager.pageAvaDataTab("app_nav_tab", "" + tabs.getHomePageId(), "nav_page_id",
				tabs.getTabs());
		if (pageIds != null) {
			String removeIds = tabs.getRemoveAssIds();
			if (removeIds != null && removeIds != "") {
				for (String id : removeIds.split("_")) {
					pageIds = addRemoveId(pageIds, id, false);
				}
			}
			String addIds = tabs.getAddAssIds();
			if (addIds != null && addIds != "") {
				for (String id : addIds.split("_")) {
					pageIds = addRemoveId(pageIds, id, true);
				}
			}
		}
		List<String> columnDataValues = new ArrayList<>();
		columnDataValues.add("" + tabs.getHomePageId());
		columnDataValues.add("" + tabs.getTabs());
		columnDataValues.add(pageIds == null ? tabs.getAddAssIds() : pageIds);
		try {
			if (pageIds == null)
				DbConnManager.getInstance().execute("app_nav_tab", tableColumns, columnDataType, columnDataValues);
			else {
				DbConnManager.getInstance().isDeleted("app_nav_tab", "" + tabs.getHomePageId(), "nav_page_id");
				DbConnManager.getInstance().execute("app_nav_tab", tableColumns, columnDataType, columnDataValues);
			}
		} catch (Exception e) {
			tabs.setError(e.getMessage());
			return tabs;
		}
		return tabs;
	}

	private String addRemoveId(String pageIds, String id, boolean isAdd) {
		id = id.trim();
		if (isAdd) {
			pageIds = pageIds + "_" + id;
		} else {
			pageIds = pageIds.replaceAll("_" + id, "").replaceAll(id + "_", "");
		}
		return pageIds;
	}
}
