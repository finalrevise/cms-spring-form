package com.javasampleapproach.formsubmission.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Utils {

	public static List<String> getTableColumns() {
		List<String> tableColumns = new ArrayList<>();
		tableColumns.add("nav_page_id");
		tableColumns.add("nav_page_description");
		tableColumns.add("nav_page_path");
		tableColumns.add("nav_page_path_id");
		tableColumns.add("nav_page_title");
		tableColumns.add("parent_page_id");
		tableColumns.add("cat");
		return tableColumns;
	}

	public static List<String> getColumnDataType() {
		List<String> columnDataType = new ArrayList<>();
		columnDataType.add("LONG");
		columnDataType.add("STRING");
		columnDataType.add("STRING");
		columnDataType.add("STRING");
		columnDataType.add("STRING");
		columnDataType.add("LONG");
		columnDataType.add("INTEGER");
		return columnDataType;
	}

	public static String getMapping(int no) {
		HashMap<Integer, String> map = new HashMap<>();
		map.put(1, "School Boards");
		map.put(2, "All Competitive Exams Previous Year Question Papers");
		map.put(3, "University");
		map.put(4, "Government Jobs");
		map.put(5, "Placement Papers");
		map.put(6, "International Competition");
		map.put(10, "Books");
		map.put(11, "University/College Info");
		map.put(13, "Notes");
		map.put(14, "Assignment");
		return map.get(no);
	}
	
	public static List<String> getTabsTableColumns() {
		List<String> tableColumns = new ArrayList<>();
		tableColumns.add("nav_page_id");
		tableColumns.add("tab");
		tableColumns.add("ass_ids");
		return tableColumns;
	}
	
	public static List<String> getTabsColumnDataType() {
		List<String> columnDataType = new ArrayList<>();
		columnDataType.add("LONG");
		columnDataType.add("INTEGER");
		columnDataType.add("STRING");
		return columnDataType;
	}

}
