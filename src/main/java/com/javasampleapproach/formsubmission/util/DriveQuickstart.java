package com.javasampleapproach.formsubmission.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Children;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.ChildList;
import com.google.api.services.drive.model.ChildReference;
import com.google.api.services.drive.model.File;
import com.javasampleapproach.formsubmission.model.NavPage;

public class DriveQuickstart {
	private static final String APPLICATION_NAME = "Google Drive API Java Quickstart";
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
	private static final String TOKENS_DIRECTORY_PATH = "tokens";

	/**
	 * Global instance of the scopes required by this quickstart. If modifying these
	 * scopes, delete your previously saved tokens/ folder.
	 */
	private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE_METADATA_READONLY);
	private static final String CREDENTIALS_FILE_PATH = "credentials.json";

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException If the credentials.json file cannot be found.
	 */
	private Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {
		// Load client secrets.
		InputStream in = DriveQuickstart.class.getResourceAsStream(CREDENTIALS_FILE_PATH);
		GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

		// Build flow and trigger user authorization request.
		GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
				clientSecrets, SCOPES)
						.setDataStoreFactory(new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH)))
						.setAccessType("offline").build();
		LocalServerReceiver receier = new LocalServerReceiver.Builder().setPort(8888).build();
		return new AuthorizationCodeInstalledApp(flow, receier).authorize("user");
	}

	public List<NavPage> main(NavPage customer) {
		try {
			List<NavPage> customers = new ArrayList<>();
			// Build a new authorized API client service.
			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
			Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT))
					.setApplicationName(APPLICATION_NAME).build();

			// Print the names and IDs for up to 10 files.
//			FileList result = service.files().list().setPageSize(100).setFields("nextPageToken, files(id, name)")
//					.execute();
//			List<File> files = result.getFiles();
//			if (files == null || files.isEmpty()) {
//				System.out.println("No files found.");
//			} else {
//				System.out.println("Files:");
//				for (File file : files) {
//					System.out.printf("%s (%s)\n", file.getName(), file.getId());
//				}
//			}
			customers = printFilesInFolderPaper(service, customer.getFolderId(), customer);
			return customers;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

//	List<Customer> printFilesInFolderSchool(Drive service, String folderId, List<Customer> customers)
//			throws IOException {
//		Children.List request = service.children().list(folderId);
//		do {
//			try {
//				ChildList children = request.execute();
//				for (ChildReference child : children.getItems()) {
//					Customer customer = new Customer();
//					List<Customer> list = new ArrayList();
//					customer = printFile(service, child.getId(), customer);
//					list = printFilesInFolderClass(service, child.getId(), customer, list);
//					customers.addAll(list);
//				}
//				request.setPageToken(children.getNextPageToken());
//			} catch (IOException e) {
//				System.out.println("An error occurred: " + e);
//				request.setPageToken(null);
//			}
//		} while (request.getPageToken() != null && request.getPageToken().length() > 0);
//		return null;
//	}

//	List<Customer> printFilesInFolderClass(Drive service, String folderId, Customer customer, List<Customer> customers)
//			throws IOException {
//		Children.List request = service.children().list(folderId);
//
//		do {
//			try {
//				ChildList children = request.execute();
//				for (ChildReference child : children.getItems()) {
//					customer = printFile(service, child.getId(), customer);
//					List<Customer> list = printFilesInFolderPaper(service, child.getId(), customer);
//					customers.addAll(list);
//				}
//				request.setPageToken(children.getNextPageToken());
//			} catch (IOException e) {
//				System.out.println("An error occurred: " + e);
//				request.setPageToken(null);
//			}
//		} while (request.getPageToken() != null && request.getPageToken().length() > 0);
//		return customers;
//	}

	List<NavPage> printFilesInFolderPaper(Drive service, String folderId, NavPage customer) throws IOException {
		Children.List request = service.children().list(folderId);
		List<NavPage> list = new ArrayList<NavPage>();
		do {
			try {
				ChildList children = request.execute();
				for (ChildReference child : children.getItems()) {
					NavPage tempCustomer = new NavPage();
					tempCustomer = printFile(service, child.getId(), customer);
					if (tempCustomer != null && tempCustomer.getPaperUrl() != null) {
						list.add(tempCustomer);
					}
				}
				request.setPageToken(children.getNextPageToken());
			} catch (IOException e) {
				System.out.println("An error occurred: " + e);
				request.setPageToken(null);
			}
		} while (request.getPageToken() != null && request.getPageToken().length() > 0);
		return list;
	}

	private NavPage printFile(Drive service, String fileId, NavPage tempCustomer) {
		NavPage customer = new NavPage();
		try {
			File file = service.files().get(fileId).execute();
//			if ("application/vnd.google-apps.folder".equalsIgnoreCase(file.getMimeType())) {
//				customer.setBoardName(tempCustomer.getBoardName());
//				customer.setExamName(tempCustomer.getBoardName());
//				customer.setClassTemp(file.getTitle().replaceAll("-", " ").replaceAll("_", " "));
//			} else {
//				customer.setExamName(tempCustomer.getBoardName());
//			customer.setBoardName(tempCustomer.getBoardName());
//			customer.setClassTemp(tempCustomer.getClassTemp());
//			customer.setSyllabus(tempCustomer.getSyllabus());
			customer.setHomePageId(tempCustomer.getHomePageId());
			customer.setTitle1(tempCustomer.getTitle1());
			customer.setTitle1Desc(tempCustomer.getTitle1Desc());
			customer.setTitle2(tempCustomer.getTitle2());
			customer.setTitle2Desc(tempCustomer.getTitle2Desc());
			customer.setTitle3(tempCustomer.getTitle3());
			customer.setTitle3Desc(tempCustomer.getTitle3Desc());
			customer.setTitle4(tempCustomer.getTitle4());
			customer.setTitle4Desc(tempCustomer.getTitle4Desc());
			customer.setTitle5(tempCustomer.getTitle5());
			customer.setTitle5Desc(tempCustomer.getTitle5Desc());
			customer.setTitle6(tempCustomer.getTitle6());
			customer.setTitle6Desc(tempCustomer.getTitle6Desc());
			customer.setCat(tempCustomer.getCat());
			customer.setPaperName(file.getTitle().replaceAll("-", " ").replaceAll("_", " ").replaceAll(".pdf", ""));
			customer.setPaperUrl(file.getAlternateLink());
//			}
			return customer;
		} catch (IOException e) {
			System.out.println("An error occured: " + e);
		}
		return null;
	}

}