package com.javasampleapproach.formsubmission.manager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created by nitinashish on 16/06/18.
 * <p>
 * Single thread model
 */
public class DbConnManager {
	private final Object mLOCK = new Object();
	private static DbConnManager instance;
	private Connection conn;
	private PreparedStatement stmt;

	private DbConnManager() {

	}

	public static DbConnManager getInstance() {
		if (instance == null) {
			synchronized (DbConnManager.class) {
				if (instance == null) {
					instance = new DbConnManager();
				}
			}
		}
		return instance;
	}

	public Connection getConnection() {
		if (conn == null) {
			synchronized (mLOCK) {
				if (conn == null) {
					String databaseEndpoint = "jdbc:mysql://localhost:3306/finalrevisedb?autoReconnect=true&useSSL=false";
					String user = "root";
					String password = "finalrevise";

					try {
						Class.forName("com.mysql.jdbc.Driver").newInstance();
						conn = DriverManager.getConnection(databaseEndpoint, user, password);
					} catch (Exception e) {
						System.out.println("Error creating db conn");
						e.printStackTrace();
					}
				}
			}
		}

		return conn;
	}

	private String createInsertSql(String table, List<String> tableColumns) {
		StringBuilder builder = new StringBuilder();
		StringBuilder args = new StringBuilder();
		builder.append("insert into ");
		builder.append(table);
		builder.append(" (");

		int maxColumnIndex = tableColumns.size() - 1;
		for (int index = 0; index <= maxColumnIndex; index++) {
			args.append("?");
			builder.append(tableColumns.get(index));

			if (index < maxColumnIndex) {
				builder.append(",");
				args.append(",");
			}
		}

		builder.append(")");
		builder.append(" values (");
		builder.append(args.toString());
		builder.append(")");
		return builder.toString();
	}

	public synchronized boolean execute(String table, List<String> tableColumns, List<String> columnDataType,
			List<String> columnDataValues) throws Exception {
		boolean success = false;

		if (tableColumns.size() == 0 || columnDataType.size() == 0 || columnDataValues.size() == 0) {
			return success;
		}

		String sql = createInsertSql(table, tableColumns);
		if (stmt == null) {
			stmt = getConnection().prepareStatement(sql);
		}

		for (int index = 0; index < tableColumns.size(); index++) {
			String dataType = columnDataType.get(index);
			String data = columnDataValues.get(index);

			if ("STRING".equalsIgnoreCase(dataType)) {
				stmt.setString(index + 1, data);
			} else if ("INT".equalsIgnoreCase(dataType)) {
				stmt.setInt(index + 1, Integer.parseInt(data));
			} else if ("INTEGER".equalsIgnoreCase(dataType)) {
				stmt.setInt(index + 1, Integer.parseInt(data));
			} else if ("LONG".equalsIgnoreCase(dataType)) {
				stmt.setLong(index + 1, Long.parseLong(data));
			} else if ("BOOLEAN".equalsIgnoreCase(dataType)) {
				stmt.setBoolean(index + 1, Boolean.parseBoolean(data));
			}
		}

		if (stmt != null) {
			int record = stmt.executeUpdate();
			success = record > 0 ? true : false;
			System.out.println(stmt.toString().replaceAll("com.mysql.cj.jdbc.ClientPreparedStatement:", "") + ";");
		}
		closeConn();

		return success;
	}

	public Long pageAvaData(String tableName, String value, String columnName) {
		// our SQL SELECT query.
		// if you only need a few columns, specify them by name instead of using "*"
		Long id = null;
		try {
			String query = "SELECT * FROM " + tableName + " where " + columnName + " = '" + value + "'";

			// create the java statement
			Statement st = getConnection().createStatement();

			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(query);

			// iterate through the java resultset
			while (rs.next()) {
				id = rs.getLong("nav_page_id");
			}
			st.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		return id;
	}

	public void closeConn() {
		if (stmt != null) {
			try {
				stmt.close();
				stmt = null;
				// System.out.println("closed statement");
			} catch (Exception e) {
				// System.out.println("error closing prepared statement");
				// e.printStackTrace();
			}
		}

		if (conn != null) {
			try {
				conn.close();
				conn = null;
				// System.out.println("closed database connection");
			} catch (Exception e) {
				// System.out.println("error closing db connection");
				// e.printStackTrace();
			}
		}
	}

	public Long pageAvaData(String tableName, String value, String columnName1, String columnName2, Long boardId) {
		// our SQL SELECT query.
		// if you only need a few columns, specify them by name instead of using "*"
		Long id = null;
		try {
			String query = "SELECT * FROM " + tableName + " where " + columnName1 + " = '" + value + "' AND "
					+ columnName2 + " = '" + boardId + "'";

			// create the java statement
			Statement st = getConnection().createStatement();

			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(query);

			// iterate through the java resultset
			while (rs.next()) {
				id = rs.getLong("nav_page_id");
			}
			st.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		return id;
	}

	public String pageAvaDataTab(String tableName, String value, String columnName, String isTab) {
		// our SQL SELECT query.
		// if you only need a few columns, specify them by name instead of using "*"
		String ids = null;
		try {
			String query = "SELECT * FROM " + tableName + " where " + columnName + " = '" + value + "' and tab = "+isTab;

			// create the java statement
			Statement st = getConnection().createStatement();

			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(query);

			// iterate through the java resultset
			while (rs.next()) {
				ids = rs.getString("ass_ids");
			}
			st.close();
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		return ids;
	}

	public boolean isDeleted(String tableName, String value, String columnName) {
		String sql = "delete from "+tableName+" where "+columnName+"="+value;

		try {
			Statement stmt = getConnection().createStatement();
			return stmt.executeUpdate(sql) > 0 ? true : false;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
